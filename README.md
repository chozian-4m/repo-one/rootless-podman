# Rootless Podman

This container is used in the Container Hardening pipeline build stage. It is a rootless build container that is used to enhance security. The image contains `podman`, `skopeo`, `buildah`, and `squid (for proxying / not running by default)`.

## Using this image

This image uses the `podman` user, with `uid` 1000.
