ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

USER 0

COPY signatures/RPM-GPG-KEY-centosofficial /etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial
COPY *.rpm /tmp/dnfmodules/

ENV _BUILDAH_STARTED_IN_USERNS="" BUILDAH_ISOLATION=chroot PATH="${PATH}:/root/.local/bin"

RUN rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial && \
    dnf upgrade -y; \
    dnf reinstall -y shadow-utils; \
    dnf install skopeo podman buildah crun -y; \
    dnf install -y /tmp/dnfmodules/*.rpm --setopt=tsflags=nodocs; \
    dnf clean all; \
    rm -rf /var/cache/dnf /tmp/dnfmodules/

RUN useradd -u 1000 -ms /bin/bash podman; \
    echo podman:100000:999999 >/etc/subuid; \
    echo podman:100000:999999 >/etc/subgid;

COPY files/podman-containers.conf /home/podman/.config/containers/containers.conf
COPY files/containers.conf /etc/containers/containers.conf

RUN chown podman:podman -R /home/podman

RUN mkdir -p /home/podman/.local/share/containers; chown podman:podman -R /home/podman

VOLUME /var/lib/containers
VOLUME /home/podman/.local/share/containers

RUN chmod 644 /etc/containers/containers.conf; sed -i -e 's/driver = "overlay"/driver = "vfs"/g' -e 's|^#mount_program|mount_program|g' -e '/additionalimage.*/a "/var/lib/shared",' -e 's|^mountopt[[:space:]]*=.*$|mountopt = "nodev,fsync=0"|g' /etc/containers/storage.conf
RUN mkdir -p /var/lib/shared/overlay-images /var/lib/shared/overlay-layers /var/lib/shared/vfs-images /var/lib/shared/vfs-layers; touch /var/lib/shared/overlay-images/images.lock; touch /var/lib/shared/overlay-layers/layers.lock; touch /var/lib/shared/vfs-images/images.lock; touch /var/lib/shared/vfs-layers/layers.lock

ENV _CONTAINERS_USERNS_CONFIGURED=""

# squid permissions
RUN chown podman:podman -R /etc/squid && \
    chown podman:podman -R /var/log/squid

USER podman

WORKDIR /home/podman
